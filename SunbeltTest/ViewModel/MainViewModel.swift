//
//  MainViewModel.swift
//  SunbeltTest
//
//  Created by jose fernando herrera montoya on 4/12/19.
//  Copyright © 2019 SunbeltSAS. All rights reserved.
//

import Foundation
import Alamofire
class MainViewModel {
    
    
    func getQuotes(main : MainProtocol ) {
        
        AF.request("http://apilayer.net/api/live?access_key=ab333a09bf068badbf2723260ceb51db&format=1")
            
            .responseJSON { response in
                                    
                let JSON = response.value as! NSDictionary
                                    
                let quotes = JSON.object(forKey: "quotes") as! [String : Double]
            
                main.onGetQuotes(quotes: quotes)
                                    
        }
        
    }
    
}


protocol MainProtocol {
    
    func onGetQuotes( quotes:[String:Double] )
    
}
