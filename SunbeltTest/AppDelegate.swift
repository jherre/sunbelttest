//
//  AppDelegate.swift
//  SunbeltTest
//
//  Created by jose fernando herrera montoya on 3/12/19.
//  Copyright © 2019 SunbeltSAS. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

 var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    
}

