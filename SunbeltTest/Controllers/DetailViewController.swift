//
//  DetailViewController.swift
//  SunbeltTest
//
//  Created by jose fernando herrera montoya on 3/12/19.
//  Copyright © 2019 SunbeltSAS. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController{
    
    @IBOutlet weak var navigationBack: UINavigationItem!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    var name: String?
    var value: String?
    
    override func viewDidLoad() {
            
        navigationBack.title = name
        valueLabel.text = value!
        
    
    }
    
}
