//
//  MainViewController.swift
//  SunbeltTest
//
//  Created by jose fernando herrera montoya on 3/12/19.
//  Copyright © 2019 SunbeltSAS. All rights reserved.
//

import Foundation
import UIKit


class MainViewController: UITableViewController, MainProtocol {
    
    @IBOutlet weak var currenciesViewList: UITableView!
    
    var quotes:[String:Double] = [:]
    var viewModel:MainViewModel!
    
    override func viewDidLoad() {
                                  
        viewModel = MainViewModel()
        viewModel.getQuotes(main: self)
        
    }
    
    func onGetQuotes(quotes: [String : Double]) {
        
        self.quotes = quotes
        currenciesViewList.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return quotes.keys.count
        
    }
      
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style:
            
            UITableViewCell.CellStyle.subtitle, reuseIdentifier: "mycell")
        
        cell.textLabel?.text  = String(Array(quotes)[indexPath.row].key.suffix(3))
             
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "uiView", sender: indexPath.row)
        
        
    }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if let viewController = segue.destination as? DetailViewController {
                
            let nameCompareCurrency = String(Array(quotes)[sender as? Int ?? 0].key.prefix(3))
            
            let currencyValue = String(Array(quotes)[sender as? Int ?? 0].value)
            
            viewController.name = String(Array(quotes)[sender as? Int ?? 0].key.suffix(3))
            
            viewController.value = "1 " + nameCompareCurrency
            
                + " = " + currencyValue + " " + viewController.name!
            
        }
        
    }
    
}
